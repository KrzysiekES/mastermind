/*jshint jquery: true, devel: true */
var mas = angular.module("mastermind", []);

mas.controller("gameCTRL", function($scope, $http){
  $scope.answers = []; //Tablica do zapisywania odpowiedzi
  $scope.nAnswers =0; // licznik do tablicy z odpowiedziami
  
  $scope.startNewGame = function(){
    //Zapytanie Ajax do funkcji odpowiedzialnej za nową grę
    $http({
      method: 'GET',
      url:'/newGame/'+ $scope.mySize + "/" + $scope.myDim + "/" 
    }).success(function(data){    
      $scope.randomNumbers = data.randomNumbers; //przypisanie losowych liczb do scope'a
      //alert(data.randomNumbers);
    });
        
      $(".settInp").prop( "disabled", true ); //blokuje inputy settingsowe
      $('#start').css("display","none"); //ukrywa przycisk rozpocznij grę
      $(".playAgain").css("display","inline"); // pokazuje przycisk zagraj jeszcze raz
      $(".gameField").css("display","block"); //pokazanie inputa z mozliwoscia wpis odp
  };
  
  $scope.playAgain = function(){
    $(".win").css("display","none"); //ukrywa win
    $(".playAgain").css("display","none"); //ukrywa przycisk zagraj jeszcze raz
    $(".lose").css("display","none"); //ukrywa lose
    $('#start').css("display","inline"); //pokazuje przycisk rozpocznij gre
    $(".settInp").prop( "disabled", false ); // powoduje ze nie mozna w czasie gry zmienic ustawien
     $(".gameField").css("display","none"); //pokazanie inputa z mozliwoscia wpis odp
    $scope.nAnswers=0; //wyzerowanie licznika odpowiedzi
    $scope.answers = [];//wyzerowanie tablicy
  }
  
  //Funkcja do zapisywania odpowiedzi
  $scope.myAnswers = function(answer,bDot,wDot){
     var ans=''; //zmienna do pokazania w innym formacie odpowiedzi
      for(var i=0 ;i<answer.length; i++){ //konwersja odp
        ans += answer[i];
      }
    var bTable = [];//zmienna do wyswietlania ilosci czarnych kolek, dla fora
    for(var i=0; i<bDot; i++){
      bTable[i]= i ;
    }
    var wTable = [];//zmienna do wyswietlania ilosci Białych kolek, dla fora
    for(var i=0; i<wDot; i++){
      wTable[i]= i ;
    }
    
    $scope.answers[$scope.nAnswers] = {
      myAnswer: ans,
      bDot: bTable,
      wDot: wTable
    };
    console.dir("Odpowiedzi: " + $scope.answers);
    $scope.nAnswers++; //zwiększenie liczby odpowiedzi
  };
  
  $scope.checkAnswer = function(){
    $http({
      method: 'GET',
      url:'/gameCheck/'+ $scope.quessNumber +'/' + $scope.randomNumbers + '/' + $scope.mySize + '/' + $scope.myDim + '/'+ $scope.myMax
    }).success(function(data){
      $scope.myMax = data.max; //aktualizacja liczby ruchow
      $scope.blackDot = data.blackDot; //czarne kropki
      $scope.whiteDot = data.whiteDot; //czarne kropki
      $scope.win = data.win;
      $scope.lose = data.lose;
      $scope.quessNumbers = data.quessNumbers;
      $scope.myAnswers($scope.quessNumbers, $scope.blackDot, $scope.whiteDot); //zapisuje odpowiedz i kropki do zmiennej
      
      if($scope.win === true) {
        $(".win").css("display","block"); //wlaczenie ekranu po wygranej
        $(".gameField").css("display","none");
      }
      if($scope.lose === true && $scope.win === false ) {
        $(".lose").css("display","block"); //wlaczenie ekranu po przegranej
        $(".gameField").css("display","none"); 
      }
      
    });
    
   $scope.quessNumber= ''; //Czyszczenie inputa z odpowiedzią
  };
  
});

