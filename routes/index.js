/*jshint node: true, esnext: true, devel: true  */

exports.index = function (req, res) {
    res.render('index', {
        title: 'Mastermind'
    });
};

exports.newGame = function(req, res) {
  console.log("Nowa Gra");
  
  var _ = require('underscore'); //Załączenie underscore'a
  var size = req.params.size; //liczba elementów w rzędzie
  var dim = req.params.dim; //zakres
  var tab =[];//tablica do przechowywania liczb do odgadnięcia
  
  //Losowanie Tablicy do odgadnięcia
  for(var i=0; i<size; i++){
    tab[i]=_.random(1,dim);
    console.log("Wylosowało: " + tab[i]);
  }
  
  var data = {
    randomNumbers: tab
  };
  
  res.send(data); //Wysłanie do Geta w formie JSONa
};

exports.gameCheck = function(req, res) {
  
  console.log("GameCheck - w trakcie gry");
  
  var size = req.params.size; //liczba elementów w rzędzie
  var max = req.params.max; //liczba maxymalnych ruchów
  
  var randomNumbers = req.params.randomNumbers.split(","); //zmiana formatu randomNumbers
  var quessNumbers = req.params.quessNumber.split(""); //zmiana formatu
  
  var blackDot = 0; //Liczba czarnych kropek
  var whiteDot = 0; //Liczba białych kropek
  
  var win = false;
  var lose = false;
  
  console.dir("Zgadywane: " + quessNumbers);
  console.dir("Poprawne: " + randomNumbers);
  
  var helperOne=[];
  var helperTwo=[];
  
  /*Wyliczanie Czarnych Kul */
  for(var x=0; x < randomNumbers.length; x++ ) {
    if(randomNumbers[x] === quessNumbers[x]){
      blackDot++;
      console.log("Czarnych kropek : " + blackDot);
      helperOne[x]=true; //oznaczenie zajetych pól
      helperTwo[x]=true; //oznaczenie zajetych pól
    }else {
      helperOne[x]=false; //oznaczenie wolnych pól
      helperTwo[x]=false; //oznaczenie wolnych pól
    }
  }
  /*Wyliczanie Białych Kul */
  for (var i = 0; i < randomNumbers.length; i++) {        
    for (var j = 0; j < randomNumbers.length; j++) {
      if(helperOne[j]===false && helperTwo[i]===false && quessNumbers[i]===randomNumbers[j]){
        helperOne[j]=true;
        helperTwo[i]=true;
        whiteDot++;
      }
    };
  };
  
  max--; //zmniejszamy liczbę ruchów
  if(max === 0){ lose = true;} //zmiana na true gdy ruchy się skończą
  if(randomNumbers.length === blackDot){  win = true;} //zmiana na true gdy wszystkie czarne
  
  var data = {
    quessNumbers: quessNumbers,
    max: max,
    blackDot: blackDot,
    whiteDot: whiteDot,
    win: win,
    lose: lose
  };

  res.send(data); //wysyłamy w postaci Jsona geta
};


